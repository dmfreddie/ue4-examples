# UE4-Examples

This repo is designed to aid in the understanding of how varius systems *COULD* be implemented via Blueprints in Unreal Engine 4.

Currently there are:
- Save every time a collectable is picked up
- Save collectables every time a checkpoint is reached
- Checkpoint system with respawning (works across game sessions)